## Chonky Blocks

An extremely WIP rewrite of an old project of mine. Written in C++ using Visual Studio Community and compiled with MSVC.


### Dependencies

* [GLFW](https://www.glfw.org/) for window management
* [glad](https://github.com/dav1dde/glad-web) for OpenGl extension loading
* [glm](https://glm.g-truc.net/0.9.9/index.html) for math
* [stb libs](https://github.com/nothings/stb) for image i/o and font loading and rendering


### Disclaimer

This is my first C++ project, and I'm still learning the language. My code is bad and I feel bad.

