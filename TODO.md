## To-Do
#### (not necessarily in order)

- ShaderProgram improvements
	* use [glGetAttribLocation](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetAttribLocation.xhtml) for vertex format attributes?
- Chunk Columns
- World Generator
- Misc. Math
	* More Ray intersections<sup>[1](https://glm.g-truc.net/0.9.9/api/a00748.html)</sup>
	* Worley noise
	* Open Simplex Noise
	* Poisson disk generation<sup>[1](https://github.com/corporateshark/poisson-disk-generator)</sup>
- NBT<sup>[1](https://minecraft.gamepedia.com/NBT_format) [2](https://github.com/acfoltzer/nbt/blob/master/NBT-spec.txt) [3](https://wiki.vg/NBT) [4](https://github.com/ljfa-ag/libnbtplusplus) [5](https://github.com/chmod222/cNBT)</sup>
	* Internal representation
	* toString, parseString
	* Compressed I/O using [zlib](https://www.zlib.net/) and [zstr](https://github.com/mateidavid/zstr)?
- Event System?
- Entities
	* Components?<sup>[1](https://github.com/skypjack/entt)</sup>
	* AI
	* Pathfinding
		- A* or Anya* or PolyAnya?
		- Generate nav-meshes on separate thread?
	* UUID/GUID type and utils?
- Input
	* Move main game loop off of main thread so it isn't blocked by glfwPollEvents() (or use custom glfw version that doesn't block main thread with glfwPollEvents()<sup>[1](https://github.com/glfw/glfw/pull/1426)</sup>)
- GUI System
	* Static images
	* 9-slice rendering
	* Text
	* Buttons
	* Sliders
	* Scrolling panels
	* Text input<sup>[1](https://github.com/nothings/stb/blob/master/stb_textedit.h)</sup>
- Lighting
	* Floodfill block lighting<sup>[1](https://www.reddit.com/r/VoxelGameDev/comments/chqyg2/voxel_shape_experimentation_lab_exploring_what/evd5076/)</sup>
	
